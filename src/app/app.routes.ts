import {RouterModule, Routes} from '@angular/router';
import {LandingComponent} from "./modules/landing/landing.component";
import {OurappComponent} from "./modules/auth/ourapp/ourapp.component";

export let APP_ROUTES: Routes = [
  {path: '',
    component: LandingComponent},
  {
    path: 'nosotros',
    component: OurappComponent,
  },
  {
    path: 'login',
    loadChildren: () => import('./modules/auth/auth.module')
      .then(m => m.AuthModule)
  },
  {
    path: 'home',
    loadChildren: () => import("./modules/panel/panel.module")
      .then(m => m.PanelModule)
  },
];
export let AppRouterModule = RouterModule.forRoot(APP_ROUTES, {
  // useHash: true,
  // initialNavigation: false
});
