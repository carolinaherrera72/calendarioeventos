import { Component, OnInit } from '@angular/core';
import { ApiService} from "../../../services/api.service";
import {Router} from "@angular/router";
import { format } from 'date-fns';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public event: any;
  vacio: any;

  constructor(private eventtiApiService:ApiService, private router: Router) { }

  ngOnInit(): void {
this.eventos()
  }
  eventos(){
    this.eventtiApiService.getEvents()
      .subscribe(
        (data:any) => { // Success
          this.event=data;
          this.vacio=this.event.length
        },
        (error) => {
          console.error(error);
        }
      );
  }
  // @ts-ignore
  calculateTotalSpend(event): number {
    let sum = 0;
    sum += (parseFloat(event.field_spend1) || 0); // Si el resultado es NaN, usa 0
    sum += (parseFloat(event.field_spend2) || 0);
    sum += (parseFloat(event.field_spend3) || 0);
    sum += (parseFloat(event.field_spend4) || 0);
    return sum;
  }
  formatDate(dateString: string, formatString: string): string {
    const date = new Date(dateString);
    return format(date, formatString);
  }
  editEvent(events: any) {
    localStorage.setItem('editEvent', JSON.stringify(events));
    this.router.navigate(['/home/edit']);

  }

  viewEvent(events: any) {
    localStorage.setItem('editEvent', JSON.stringify(events));
    this.router.navigate(['/home/view']);
  }

  deleteEvent(uuid: any) {
    this.eventtiApiService.deleteEventFunction(uuid)
      .subscribe(
        (data:any) => { // Success
          this.eventos()
        },
        (error) => {
          console.error(error);
        }
      );
  }
}
