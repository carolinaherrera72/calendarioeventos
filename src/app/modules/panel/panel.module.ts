import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { PanelRoutingModule} from "./panel-routing.module";
import {AuthModule} from "../auth/auth.module";
import { CreateEventComponent } from './create-event/create-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import { ViewEventComponent } from './view-event/view-event.component';
import {UserDataComponent} from "../auth/user-data/user-data.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FormatNumberPipe} from "../../format-number.pipe";


@NgModule({
  declarations: [
    HomeComponent,
    CreateEventComponent,
    EditEventComponent,
    ViewEventComponent
  ],
    imports: [
        CommonModule,
        PanelRoutingModule,
        AuthModule,
        UserDataComponent,
        FormsModule,
        ReactiveFormsModule,
        FormatNumberPipe
    ]
})
export class PanelModule { }
