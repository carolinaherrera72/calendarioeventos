import { Component, OnInit } from '@angular/core';
import { ApiService} from "../../../services/api.service";
import {Router} from "@angular/router";
import moment from "moment/moment";

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.css']
})
export class EditEventComponent implements OnInit {
  public event: any;
  private evenTemp: any;
  eventDataTemp: any;
  eventData: any;

  constructor(private eventtiApiService:ApiService, private router: Router) { }

  ngOnInit(): void {
    this.evenTemp=localStorage.getItem('editEvent')
    this.eventDataTemp=[JSON.parse(this.evenTemp)][0];
    console.log(this.eventDataTemp)
    this.eventData = {
      title: this.eventDataTemp.title,
      field_inicio:   this.eventDataTemp.field_inicio,
      field_fin: this.eventDataTemp.field_fin,
      field_description: this.eventDataTemp.field_description,
      field_estado: this.eventDataTemp.field_estado,
      field_spend1: this.eventDataTemp.field_spend1,
      field_spend2: this.eventDataTemp.field_spend2,
      field_spend3: this.eventDataTemp.field_spend3,
      field_spend4: this.eventDataTemp.field_spend4,
      field_type: this.eventDataTemp.field_type,
    };
  }
  formatDateForInput(dateTimeStr: string): string {
    // Comprueba si la cadena está vacía o es nula
    if (!dateTimeStr) {
      console.error('Provided date is empty or null.');
      return '';
    }

    const date = new Date(dateTimeStr);

    // Comprueba si la fecha es válida
    if (isNaN(date.getTime())) {
      console.error('Invalid date provided:', dateTimeStr);
      return '';
    }

    return date.toISOString().slice(0, 16); // Formatea la fecha para el input
  }
  cleanObject(obj:any) {
    const newObj = Array.isArray(obj) ? [] : {};
    Object.keys(obj).forEach(key => {
      if (obj[key] && typeof obj[key] === 'object') {
        const value = this.cleanObject(obj[key]); // Recursivamente limpia los objetos
        if (Object.keys(value).length !== 0) { // Solo añade el objeto si no está vacío después de la limpieza
          // @ts-ignore
          newObj[key] = value;
        }
      } else if (obj[key] !== '') { // Añade propiedades que no son objetos y no están vacías
        // @ts-ignore
        newObj[key] = obj[key];
      }
    });
    return newObj;
  }
  editEvent(){
    const inicio =  (document.getElementById('field_inicio') as HTMLInputElement).value
    const fin =  (document.getElementById('field_fin') as HTMLInputElement).value
    const evento = {
      data: {
        type: "node--events",
        id:this.eventDataTemp.uuid,
        attributes: {
          "title":  (document.getElementById('title') as HTMLInputElement).value,
          "field_inicio": moment(inicio).format("YYYY-MM-DDTHH:mm:ssZ"),
          "field_fin": moment(fin).format("YYYY-MM-DDTHH:mm:ssZ"),
          "field_description":  (document.getElementById('field_description') as HTMLInputElement).value,
          "field_links": {
            "uri":  (document.getElementById('field_estado') as HTMLInputElement).value,
            "title": "Enlace"
          },
          "field_spend1":  (document.getElementById('field_spend1') as HTMLInputElement).value,
          "field_spend2":  (document.getElementById('field_spend2') as HTMLInputElement).value,
          "field_spend3":  (document.getElementById('field_spend3') as HTMLInputElement).value,
          "field_spend4":  (document.getElementById('field_spend4') as HTMLInputElement).value,
          "field_type":  (document.getElementById('field_type') as HTMLInputElement).value
        }
      }
    };
    let cleanedData = this.cleanObject(evento);
    this.eventtiApiService.editEventFunction(cleanedData,this.eventDataTemp.uuid)
      .subscribe(
        (data:any) => { // Success
          this.router.navigate(['/home']);
        },
        (error) => {
          console.error(error);
        }
      );
  }
}
