import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {format} from "date-fns";

@Component({
  selector: 'app-view-event',
  templateUrl: './view-event.component.html',
  styleUrls: ['./view-event.component.css']
})
export class ViewEventComponent implements OnInit {
  private evenTemp: any;
  public event: any;

  constructor( private router: Router) { }

  ngOnInit(): void {
    this.evenTemp=localStorage.getItem('editEvent')
    this.event=JSON.parse(this.evenTemp)
  }
  formatDate(dateString: string, formatString: string): string {
    const date = new Date(dateString);
    return format(date, formatString);
  }

  editEvent() {
    this.router.navigate(['/home/edit']);
  }

}
