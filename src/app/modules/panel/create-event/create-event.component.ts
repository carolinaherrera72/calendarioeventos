import { Component, OnInit } from '@angular/core';
import { ApiService} from "../../../services/api.service";
import moment from 'moment';
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent {

  constructor(private eventtiApiService:ApiService, private router: Router) { }
  eventData = {
    title: '',
    field_inicio: '',
    field_fin: '',
    field_description: '',
    field_estado: '',
    field_spend1: '',
    field_spend2: '',
    field_spend3: '',
    field_spend4: '',
    field_type: ''
  };
  cleanObject(obj:any) {
    const newObj = Array.isArray(obj) ? [] : {};
    Object.keys(obj).forEach(key => {
      if (obj[key] && typeof obj[key] === 'object') {
        const value = this.cleanObject(obj[key]); // Recursivamente limpia los objetos
        if (Object.keys(value).length !== 0) { // Solo añade el objeto si no está vacío después de la limpieza
          // @ts-ignore
          newObj[key] = value;
        }
      } else if (obj[key] !== '') { // Añade propiedades que no son objetos y no están vacías
        // @ts-ignore
        newObj[key] = obj[key];
      }
    });
    return newObj;
  }
  newEvent(){
    const evento = {
      data: {
        type: "node--events",
        attributes: this.eventData
      }
    };
    let cleanedData = this.cleanObject(evento);
    this.eventtiApiService.createEventFunction(cleanedData)
      .subscribe(
        (data:any) => { // Success
          this.router.navigate(['/home']);

        },
        (error) => {
          console.error(error);
        }
      );
  }
}
