import { NgModule } from '@angular/core';
import { HomeComponent} from "./home/home.component";
import { RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "../auth/auth.guard";
import {CreateEventComponent} from "./create-event/create-event.component";
import {EditEventComponent} from "./edit-event/edit-event.component";
import {ViewEventComponent} from "./view-event/view-event.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'create',
    component: CreateEventComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'edit',
    component: EditEventComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'view',
    component: ViewEventComponent,
    canActivate: [AuthGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanelRoutingModule { }
