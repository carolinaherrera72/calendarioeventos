import {Component, Input} from '@angular/core';
import {ApiService} from "../../../services/api.service";
import {Router} from "@angular/router";
import {FormatNumberPipe} from "../../../format-number.pipe";

@Component({
  selector: 'app-spend',
  standalone: true,
  imports: [
    FormatNumberPipe
  ],
  templateUrl: './spend.component.html',
  styleUrl: './spend.component.css'
})
export class SpendComponent {
  public events: any;
  totalOtros: any;
  totalTransporte: any;
  totalBoletas: any;
  totalViaticos: any;
  constructor(private eventtiApiService:ApiService, private router: Router) {
    this.eventtiApiService.getEvents()
      .subscribe(
        (data:any) => { // Success
          this.events=data;
          interface Gasto {
            field_spend1: string;
            field_spend2: string;
            field_spend3: string;
            field_spend4: string;
          }
          this.totalOtros = 0;
          this.totalTransporte = 0;
          this.totalBoletas = 0;
          this.totalViaticos = 0;

          this.events.forEach((gasto: Gasto) => {
            this.totalBoletas += parseFloat(gasto.field_spend1) || 0;
            this.totalTransporte += parseFloat(gasto.field_spend2) || 0;
            this.totalViaticos += parseFloat(gasto.field_spend3) || 0;
            this.totalOtros += parseFloat(gasto.field_spend4) || 0;
          });

        },
        (error) => {
          console.error(error);
        }
      );

  }
}
