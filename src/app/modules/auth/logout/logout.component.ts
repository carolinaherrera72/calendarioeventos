import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-logout',
  standalone: true,
  imports: [
    NgIf
  ],
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {
  view: any;
  token: any;

  constructor(private router: Router) { }

  logout() {
      localStorage.clear();
      this.router.navigate(['/']);
      this.view=false;
  }
}
