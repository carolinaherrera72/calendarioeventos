import {Component, OnInit} from '@angular/core';
import {Router, RouterLink} from "@angular/router";
import {ApiService} from "../../../services/api.service";
import {SpendComponent} from "../spend/spend.component";
import {NgOptimizedImage} from "@angular/common";
import {LogoutComponent} from "../logout/logout.component";


@Component({
  selector: 'app-user-data',
  standalone: true,
    imports: [
        RouterLink,
        SpendComponent,
        NgOptimizedImage,
        LogoutComponent
    ],
  templateUrl: './user-data.component.html',
  styleUrl: './user-data.component.css'
})
export class UserDataComponent implements OnInit{
  frases: string[] = [
    "Explora más allá, viaja y vive. ¡El mundo espera!",
    "Corre hacia tus sueños, viaja a tu ritmo.",
    "Suda, sonríe, repite. ¡El mundo es tuyo!",
    "Cada paso en tu viaje cuenta, ¡no pares!",
    "Mueve tu cuerpo, mueve tu mente, recorre el mundo.",
    "Viaja lejos, corre fuerte, vive pleno.",
    "Aventura y deporte, el combo perfecto para el alma.",
    "Desafíate a ti mismo, descubre nuevos lugares.",
    "Ponle pasión al deporte, ponle magia a tu viaje.",
    "Entrena fuerte, viaja lejos, vive intensamente."
  ];

  fraseAleatoria: string = '';
   user: any;
   url: any;
   events: any;
   name: any;
  constructor(private eventtiApiService:ApiService, private router: Router,) {
    this.mostrarFraseAleatoria();

  }
  mostrarFraseAleatoria(): void {
    const index = Math.floor(Math.random() * this.frases.length);
    this.fraseAleatoria = this.frases[index];
  }
  ngOnInit(): void {
      const img =localStorage.getItem('avatar');
      if(img){
          this.url=img;
      }
    this.eventtiApiService.getUser()
      .subscribe(
        (data:any) => { // Success
          this.user=data[0];
          this.name= this.user.name;
              this.url='/assets/img/'+this.user.field_avatar+'.jpg'
              localStorage.setItem('avatar',this.url);
        },
        (error) => {
          console.error(error);
        }
      );
      this.eventtiApiService.statusUserFunction()
          .subscribe(
              (data:any) => { // Success

              },
              (error) => {

              }
          );
  }

}
