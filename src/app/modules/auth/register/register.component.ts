import { Component } from '@angular/core';
import {ApiService} from "../../../services/api.service";
import {Router, RouterLink} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {NgForOf} from "@angular/common";
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {SuccessModalComponent} from "../../../core/success-modal/success-modal.component";
import {ErrorModalComponent} from "../../../core/error-modal/error-modal.component";

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [
    FormsModule,
    RouterLink,
    NgForOf,
    MatDialogModule
  ],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  private name: any;
  private pass: any;
  private email: any;
  private avatar: any;
  private error: any;
  avatars = [
    { value: 'Nutria', src: '../assets/img/Nutria.jpg', label: 'Nutria' },
    { value: 'Zorro', src: '../assets/img/Zorro.jpg', label: 'Zorro' },
    { value: 'Gato', src: '../assets/img/Gato.jpg', label: 'Gato' }
  ];

  selectedAvatar: string | undefined;

  selectAvatar(value: string) {
    this.selectedAvatar = value;
  }
  constructor(private eventtiApiService:ApiService, private router: Router,public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  createUser():void{
    this.name = (document.getElementById('name') as HTMLInputElement).value;
    this.pass = (document.getElementById('password') as HTMLInputElement).value;
    this.email = (document.getElementById('email') as HTMLInputElement).value;
    this.avatar = this.selectedAvatar
      console.log( this.avatar )
    this.eventtiApiService.registerUserFunction(this.name,this.pass,this.email,this.avatar )
      .subscribe(
        (response:any) => { // Success
          if(response.data.id){
            const dialogRef = this.dialog.open(SuccessModalComponent);
            dialogRef.afterClosed().subscribe(() => {
              this.router.navigate(['/login']);
            });
          }
          },
        (error) => {
          this.error=error.error;
          let errorMessage = '';
          this.error.errors.forEach((error: { detail: string; }) => {
            errorMessage += error.detail + '\n';
          });
          this.dialog.open(ErrorModalComponent, {
            data: { message: errorMessage }
          });
        }
      );
  }
}
