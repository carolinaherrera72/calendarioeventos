import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../../services/api.service";
import {Router, RouterLink} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {ErrorModalComponent} from "../../../core/error-modal/error-modal.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    RouterLink
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit{

  private user: any;
  private pass: any;
  private error: any;

  constructor(private eventtiApiService:ApiService, private router: Router,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getStatus();
  }
  validateUser():void{
    this.user = (document.getElementById('name') as HTMLInputElement).value;
    this.pass = (document.getElementById('password') as HTMLInputElement).value;
    this.eventtiApiService.validateUserFunction(this.user,this.pass)
      .subscribe(
        (data:any) => { // Success
          if (data){
            localStorage.setItem('csrf_token', data.csrf_token);
            localStorage.setItem('logout_token', data.logout_token);
            this.router.navigate(['/home']);
            if(data.csrf_token){
              localStorage.setItem('user', this.user);
              localStorage.setItem('pass', this.pass);
            }
          }
        },
        (error) => {
          this.error=error.message;

          this.dialog.open(ErrorModalComponent, {
            data: { message:'Verifica tu nombre de usuario y clave, no ingreses con el correo' }
          });
        }
      );
  }
  getStatus(){
    this.eventtiApiService.statusUserFunction()
      .subscribe(
        (data:any) => { // Success
          console.log(data)
        },
        (error) => {

        }
      );
  }

}
