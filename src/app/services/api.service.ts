import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private  http: HttpClient) { }
  private apiUrl = "https://www.skincareorigenes.com/blog/";
  private validateUser =  this.apiUrl + 'user/login?_format=json';
  private registerUser =  this.apiUrl + 'jsonapi/user/user';
  private statusUser =  this.apiUrl + 'user/login_status?_format=json';
  private getEvent =  this.apiUrl + 'events';
  private getData =  this.apiUrl + 'useractual';
  private newEvent =  this.apiUrl + 'jsonapi/node/events';
  private deleteEvent =  this.apiUrl + 'jsonapi/node/events/';
  private getAnonimo =  this.apiUrl + 'session/token';
  private headers = new HttpHeaders({
    'Content-Type': 'application/vnd.api+json'
  });
  private headersToken: HttpHeaders | undefined;
  validateUserFunction(user:any,pass:any) {

    const data = {
      "name": user,
      "pass": pass
    }
    const headers = new HttpHeaders({
      'Content-Type': 'application/vnd.api+json',
    });
    return this.http.post(this.validateUser, data, { headers: headers,withCredentials: true  });
  }
  registerUserFunction(user:any,pass:any, email:any, avatar:any) {
    const dataTem = {
      type: "user--user",
        attributes: {
        name: user,
        mail: email,
        pass: pass,
        status:1,
        field_avatar:avatar
      }
    }
    const userData = {
      data:dataTem
    }
    const base64Credentials = btoa('creador' + ':' + 'D9yVmh4yVcRP2Be');
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + base64Credentials,
      'Content-Type': 'application/vnd.api+json'
    });
    return this.http.post(this.registerUser, userData, { headers:headers,withCredentials: true  });
  }

  statusUserFunction() {
    return this.http.get(this.statusUser,{ headers: this.headers,withCredentials: true  });
  }
  getEvents() {
    const username = localStorage.getItem('user');
    const password =  localStorage.getItem('pass');
    const base64Credentials = btoa(username + ':' + password);
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + base64Credentials,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.getEvent, { headers });
  }
  getUser() {
    const username = localStorage.getItem('user');
    const password =  localStorage.getItem('pass');
    const base64Credentials = btoa(username + ':' + password);
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + base64Credentials,
      'Content-Type': 'application/json'
    });
    return this.http.get(this.getData, { headers });
  }
  createEventFunction(data: any){
    const username = localStorage.getItem('user');
    const password =  localStorage.getItem('pass');
    const token =  localStorage.getItem('csrf_token');
    const base64Credentials = btoa(username + ':' + password);
    if (token !== null) {
      this.headersToken = new HttpHeaders({
        'Authorization': 'Basic ' + base64Credentials,
        'Content-Type': 'application/vnd.api+json',
        'X-CSRF-Token': token
      });
    }
    return this.http.post(this.newEvent, data, { headers: this.headersToken,withCredentials: true  });

  }
  deleteEventFunction(uuid: any){
    const username = localStorage.getItem('user');
    const password =  localStorage.getItem('pass');
    const token =  localStorage.getItem('csrf_token');
    const base64Credentials = btoa(username + ':' + password);
    if (token !== null) {
      this.headersToken = new HttpHeaders({
        'Authorization': 'Basic ' + base64Credentials,
        'Content-Type': 'application/vnd.api+json',
        'X-CSRF-Token': token
      });
    }
    return this.http.delete(this.deleteEvent+uuid, { headers: this.headersToken,withCredentials: true  });

  }
  editEventFunction(data: any, uuid: string){
    const username = localStorage.getItem('user');
    const password =  localStorage.getItem('pass');
    const token =  localStorage.getItem('csrf_token');
    const base64Credentials = btoa(username + ':' + password);
    if (token !== null) {
      this.headersToken = new HttpHeaders({
        'Authorization': 'Basic ' + base64Credentials,
        'Content-Type': 'application/vnd.api+json',
        'X-CSRF-Token': token
      });
    }
    return this.http.patch(this.newEvent+'/'+uuid, data, { headers:  this.headersToken,withCredentials: true  });
  }
}
