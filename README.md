# Calendario de eventos personales con calculadora gastos UNIR

Este trabajo presenta el desarrollo de una aplicación web de página única (SPA) utilizando el
framework Angular.
La aplicación consiste en un calendario de eventos que permite a los usuarios crear y gestionar
eventos a los que asistirán, así como realizar un seguimiento de los gastos asociados a cada
evento.ademas
El objetivo principal es proporcionar una herramienta útil para planificar y controlar los costos
relacionados con la participación en eventos.

#  INTERFACES
<img src="/src/assets/img/PantallaInicioOpcion1.png?ref_type=heads" alt="">
<img src="/src/assets/img/PantallaEventos.png?ref_type=heads" alt="">

